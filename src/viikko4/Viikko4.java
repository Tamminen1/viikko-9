/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko4;
import java.util.Scanner;
/** Date 9.11.2016
 * op 0437786
 * @author Juha Tamminen
 */
public class Viikko4 {
    public static void main(String[] args) {
        String temp;
        int x;
        BottleDispenser d1 = BottleDispenser.getInstance();
        while(true) {
            Scanner scan = new Scanner(System.in);
            System.out.print("\n*** LIMSA-AUTOMAATTI ***\n" +
"1) Lisää rahaa koneeseen\n" +
"2) Osta pullo\n" +
"3) Ota rahat ulos\n" +
"4) Listaa koneessa olevat pullot\n" +
"0) Lopeta\n" +
"Valintasi: ");
            temp = scan.nextLine();
            if(temp.equals("1")==true) {
                d1.addMoney();
            }
            else if(temp.equals("2")==true) {
                d1.list();
                System.out.print("Valintasi: ");
                x = scan.nextInt();
                d1.buyBottle(x);
            }
            else if(temp.equals("3")==true) {
                d1.returnMoney();
            }
            else if(temp.equals("4")==true) {
                d1.list();
            }
            else if(temp.equals("0")==true) {
                break;
            }
        }
    }  
}
