/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko4;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
/**
 *
 * @author Juha
 */
public class BottleDispenser {
    private int bottles;
    private float money=0;
    private int x2;
    private int x3;
    private String xx;
    private String kk;
    Bottle kuitti = null; 
    private ArrayList<Bottle> bottle_array = new ArrayList();
    
    static private BottleDispenser bd = null;

    private BottleDispenser() {
        bottles = 6;
        money = 0;
        bottle_array.add(0,new Bottle("Pepsi Max",0.5,1.8));
        bottle_array.add(1,new Bottle("Pepsi Max",1.5,2.2));
        bottle_array.add(2,new Bottle("Coca-Cola Zero",0.5,2.0));
        bottle_array.add(3,new Bottle("Coca-Cola Zero",1.5,2.5));
        bottle_array.add(4,new Bottle("Fanta Zero",0.5,1.95));
        bottle_array.add(5,new Bottle("Fanta Zero",0.5,1.95));
    }
    
    static public BottleDispenser getInstance() {
        if(bd==null)
            bd = new BottleDispenser();
        return bd;
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    public void addMoney2(int x) {
        money += x;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int x) {
        x2 = 0;
        x3 = 0;
        if(x<=bottle_array.size()) {
            if(bottle_array.get(x-1)!=null) {
                for(int i=0;x2<x;i++) {
                    if(bottle_array.get(i)!=null) {
                        x2++;
                    }
                    x3=i;
                }
                if(bottles==0) {
                    System.out.println("Pullot loppu!");
                }
                else if(money<bottle_array.get(x3).getPrice()) {
                        System.out.println("Syötä rahaa ensin!");
                }
                else {
                    bottles -= 1;
                    System.out.println("KACHUNK! " + bottle_array.get(x3).getName() + " tipahti masiinasta!");
                    money -= bottle_array.get(x3).getPrice();
                    kuitti = bottle_array.get(x3);
                    bottle_array.remove(x3);
                }
            }
        }
    }
    
    public void returnMoney() {
        kk = String.format("%.2f", money);
        kk = kk.replace(".", ",");
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + kk + "€");
        money = 0;
    }
    
    public void list() {
        for(int i=1; i<=bottle_array.size(); i++) {
            System.out.println(i + "." + " Nimi: " + bottle_array.get(i-1).getName() + "\n" + "	" + "Koko: " + bottle_array.get(i-1).getSize() + "	" + "Hinta: " + bottle_array.get(i-1).getPrice());
        }
    }
    public String list2() {
        xx = null;
        for(int i=1; i<=bottle_array.size(); i++) {
            if(xx==null) {
                xx = i + "." + " Nimi: " + bottle_array.get(i-1).getName() + "\n" + "	" + "Koko: " + bottle_array.get(i-1).getSize() + "	" + "Hinta: " + bottle_array.get(i-1).getPrice() + "\n";
            }
            else {
                xx = xx + i + "." + " Nimi: " + bottle_array.get(i-1).getName() + "\n" + "	" + "Koko: " + bottle_array.get(i-1).getSize() + "	" + "Hinta: " + bottle_array.get(i-1).getPrice() + "\n";
            }
        }
        return xx;
    }
    
    public void kuitti() throws IOException {
        if(kuitti!=null) {
            BufferedWriter br = new BufferedWriter(new FileWriter("kuitti.txt"));
            br.write("Kuitti \n" + kuitti.getName() + " " + kuitti.getSize() + "l " + kuitti.getPrice() + "€");
            br.close();
        }
    }
}