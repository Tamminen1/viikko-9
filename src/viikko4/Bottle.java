/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko4;
/**
 *
 * @author Juha
 */
public class Bottle {
    private final String name;
    private final double size;
    private final double price;
    
    public Bottle(String x, double y, double z) {
        name = x;
        size = y;
        price = z;
    }
    public String getName() {
        return name;
    }
    public double getSize() {
        return size;
    }
    public double getPrice() {
        return price;
    }
}
