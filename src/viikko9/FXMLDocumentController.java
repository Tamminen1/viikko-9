/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import viikko4.BottleDispenser;
//import viikko4.Bottle;
/**
 *
 * @author Juha
 */
public class FXMLDocumentController implements Initializable {
    BottleDispenser d1 = BottleDispenser.getInstance();
    @FXML
    private Label label;
    @FXML
    private Label label2;
    @FXML
    private Slider slider;

    @FXML
    private void kuitti(ActionEvent event) throws IOException {
        d1.kuitti();
    }
    
    @FXML
    private void sliderChange(javafx.scene.input.MouseEvent event) {
        label2.setText(String.valueOf(slider.getValue()));
    }
    
    @FXML
    private void money(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.addMoney2((int) slider.getValue());
        label.setText(d1.list2());
        slider.setValue(0);
    }
    
    @FXML
    private void moneyBack(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.returnMoney();
    }
    
    @FXML
    private void button1(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(1);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button2(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(2);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button3(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(3);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button4(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(4);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button5(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(5);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button6(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(6);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button7(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(7);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button8(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(8);
        label.setText(d1.list2());
    }
    
    @FXML
    private void button9(ActionEvent event) {
        System.out.println("You clicked me!");
        d1.buyBottle(9);
        label.setText(d1.list2());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  

}
